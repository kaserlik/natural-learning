<?php

require __DIR__ .'/app/boot.php';

// Get units to print
$units = getUnits();

?>


<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Page 1</title>

	<!-- Main Styles -->
	<link rel="stylesheet" href="css/build/styles.css">

	<!-- Vendor scripts -->
	<script src="js/build/required-scripts.js"></script>

	<!-- Natural learning scripts -->
	<script src="js/build/natural-learning.js"></script>
</head>
<body>

<!-- Primary fixed Header -->
<header class="primary-header fixed">
	<div class="container">

		<!-- Logo -->
		<a href="#" class="logo"></a>

		<!-- Hamburger menu responsive -->
		<button class="hamburger hamburger--elastic" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
		</button>

		<hr class="separator">

		<!-- Search box -->
		<div class="search-box">
			<input type="text" class="form-control input-search input-sm">
		</div>

		<!-- Navigation links -->
		<nav class="primary-navigation">
			<ul class="nav nav-pills">
				<li role="presentation" class="active"><a href="#">Boxes</a></li>
				<li role="presentation"><a href="#">My Boxes</a></li>
				<li role="presentation"><a href="#">Cuestiones</a></li>
			</ul>
		</nav>

	</div>
</header>

<!-- Content -->
<section id="main-wrap" class="content">

	<!-- Heading -->
	<section class="heading-primary">
		<div class="container"><h3>Novedades</h3></div>
	</section>


	<div class="container">
		<div class="row">

			<!-- units -->
			<?php foreach ( $units['units'] as $unit ): ?>

				<?php #d($unit) ?>

				<!-- Video Thumbnail -->
				<div class="video-thumbnail-container col-md-3 col-sm-4 col-xs-12">
					<div class="video-thumbnail">

						<div class="thumbnail-content">
							<div class="img-humbnail">
								<img src="<?php echo $unit['img']['-src'] ?>" alt="video cover" class="img-responsive">
							</div>

							<div class="heading">
								<?php echo $unit['title'] ?>
							</div>

							<div class="content">

								<div class="desc"><?php echo $unit['desc'] ?></div>


								<div class="text-center">
									<a class="btn btn-primary btn-radius btn-inverted" href="#">
										<span>INSTALAR</span>
									</a>
								</div>
							</div>

						</div>
					</div>
				</div>

			<?php endforeach; ?>
		</div>
	</div>
</section>

<!-- Primary Footer -->
<footer class="primary-footer">
	<div class="container">
		<a class="logo" href="#"></a>
	</div>
</footer>


</body>
</html>

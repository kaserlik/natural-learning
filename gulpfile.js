var themePath = './',
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    gulp = require('gulp'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del'),
    minify = require('gulp-minify'),
    plumberErrorHandler = {
        errorHandler: notify.onError({
            title: 'Gulp',
            message: 'Error: <%= error.message %>'
        })
    };


/*
 |------------------------------------------------------------------
 |  File cleaners
 |------------------------------------------------------------------
 */

// Css
function cleanCompiledCss() {
    return del(themePath + 'css/build');
}

// Js
function cleanCompiledJs() {
    return del([themePath + 'js/build']);
}

/*
 |------------------------------------------------------------------------------------------------------
 |  SASS
 |------------------------------------------------------------------------------------------------------
 */

function compileSass() {
    gulp.src(themePath + 'css/src/**/*.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(themePath + 'css/build'))
        .pipe(notify({message: 'Styles compiled!'}));
}

/*
 |------------------------------------------------------------------------------------------------------
 |  MINIFY CSS
 |------------------------------------------------------------------------------------------------------
 */
function minifyCss() {
    gulp.src(themePath + 'css/build/**/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css/build'));
}

/*
 |------------------------------------------------------------------------------------------------------
 |  javaScript
 |------------------------------------------------------------------------------------------------------
 */

function compileJs() {

    // Javascript required files
    gulp.src(
        [
            // jQuery,
            'vendor/jquery-1.12.3.js',
            // Bootstrap
            'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js'

            // Bootstrap
            //'vendor/select2-4.0.2/dist/js/select2.js'
        ])
        .pipe(plumber(plumberErrorHandler))
        .pipe(jshint())
        .pipe(jshint.reporter('fail'))
        .pipe(concat('required-scripts.js'))
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.min.js', '-min.js']
        }))
        .pipe(gulp.dest(themePath + 'js/build'))
        .pipe(notify({message: 'Vendor JS files compiled!'}));

    // Theme script files
    gulp.src(
        [themePath + 'js/src/**/*.js'])
        .pipe(plumber(plumberErrorHandler))
        .pipe(jshint())
        .pipe(jshint.reporter('fail'))
        .pipe(concat('natural-learning.js'))
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.min.js', '-min.js']
        }))
        .pipe(gulp.dest(themePath + 'js/build'))
        .pipe(notify({message: 'Theme JS compiled!'}));
}


/*
 |------------------------------------------------------------------------------------------------------
 |  Fonts
 |------------------------------------------------------------------------------------------------------
 */

function publishBootstrapFonts() {
    return gulp.src('node_modules/bootstrap-sass/assets/fonts/bootstrap/*')
        .pipe(gulp.dest(themePath + '/fonts/bootstrap'))
        .pipe(notify("Bootstrap fonts ready!"));
}

/*
 |------------------------------------------------------------------------------------------------------
 |  Watcher
 |------------------------------------------------------------------------------------------------------
 */

function watcher() {
    gulp.watch(themePath + 'css/src/**/*', ['cleanCss', 'sass']);
    gulp.watch(themePath + 'js/src/**/*.js', ['cleanJS', 'js']);
}


/**
 * Gulp Tasks
 */

//Js
gulp.task('js', compileJs);
gulp.task('cleanJS', cleanCompiledJs);

//CSS/SASS
gulp.task('sass', compileSass);
gulp.task('css', minifyCss);
gulp.task('cleanCss', cleanCompiledCss);

// Fonts
gulp.task('fonts', publishBootstrapFonts);

// Watcher
gulp.task('watch', watcher);

// Init theme files
gulp.task('init', ['sass', 'js', 'fonts', 'css']);

// Default task
gulp.task('default', ['watch']);
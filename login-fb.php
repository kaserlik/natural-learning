<?php

require "app/boot.php";

if ( ! getFbAccessToken() ) {

	echo '<a href="' . htmlspecialchars( getFbLoginUrl() ) . '">Log in with Facebook!</a>';

} else {

	d( getFbLoggedUser() );
}
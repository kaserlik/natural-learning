<?php

// App url
define( 'APP_URL', 'http://natural-learning.dev' );

// Facebook API credentials
define( 'FB_APP_ID', '234387040258775' );
define( 'FB_APP_SECRET', 'adea4bfd4c37fa90b06a41d0006f20e3' );
define( 'DEFAULT_GRAPH_VERSION', 'v2.6' );
define( 'FB_LOGIN_CALLBACK', APP_URL . ' /app/fb-login.php' );
define( 'NL_SERVICE_URL', APP_URL . '/app/response.xml' );
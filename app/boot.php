<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

/*
|------------------------------------------------------------------------------
| Config
|------------------------------------------------------------------------------
*/

require "config.php";

//Session should be active
session_start();


/*
|------------------------------------------------------------------------------
| Include required files
|------------------------------------------------------------------------------
*/

// vendors
require "vendor/autoload.php";

// helpers
$directory   = new RecursiveDirectoryIterator(  __DIR__ . '/helpers' );
$recIterator = new RecursiveIteratorIterator( $directory );
$regex       = new RegexIterator( $recIterator, '/\/*.php$/i' );

foreach ( $regex as $item ) {
	if ( $item->getExtension() == 'php' ) {
		require $item->getPathname();
	}
}



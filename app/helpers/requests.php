<?php

use Curl\Curl;
use MarkWilson\XmlToJson\XmlToJsonConverter;

function getUnits() {

	$args = array(
		'os'  => 'Mac',
		'cmd' => 'last',
		'vd'  => 'NL',
		'u'   => '123456789',
		'ver' => '1.1'
	);

	$converter      = new XmlToJsonConverter();
	$array_response = $converter->asArray( new SimpleXMLElement( makeGetRequest( $args ) ) );

	return array(
		'k'     => $array_response['units']['-k'],
		'units' => $array_response['units']['unit'],
	);
}

function makeGetRequest( $args ) {
	$curl = new Curl();
	$curl->get( NL_SERVICE_URL, $args );

	return $curl->response;
}
<?php

/**
 * Build Facebook instance
 *
 * @return \Facebook\Facebook
 */
function facebook() {

	// Start Facebook App
	return new Facebook\Facebook( array(
		                              'app_id'                => FB_APP_ID,
		                              'app_secret'            => FB_APP_SECRET,
		                              'default_graph_version' => DEFAULT_GRAPH_VERSION,
	                              ) );
}


/**
 * @return bool
 */
function getFbAccessToken() {
	return ( isset( $_SESSION['fb_access_token'] ) ) ? $_SESSION['fb_access_token'] : false;
}


/**
 * Retrieve the Facebook logged user
 *
 * @return \Facebook\GraphNodes\GraphUser
 */
function getFbLoggedUser() {

	$fb = facebook();

	// Sets the default fallback access token so we don't have to pass it to each request
	$fb->setDefaultAccessToken( getFbAccessToken() );

	try {
		$response = $fb->get( '/me?fields=id,name,email,picture' );
		$userNode = $response->getGraphUser();
	}
	catch ( Facebook\Exceptions\FacebookResponseException $e ) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	}
	catch ( Facebook\Exceptions\FacebookSDKException $e ) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}

	return $userNode;
}


/**
 * Get Login URL
 *
 * @return string
 */
function getFbLoginUrl() {
	$helper      = facebook()->getRedirectLoginHelper();
	$permissions = array( 'email', 'public_profile' );

	return $helper->getLoginUrl( FB_LOGIN_CALLBACK, $permissions );
}
<?php require "app/boot.php"; ?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Page 1</title>

	<!-- Main Styles -->
	<link rel="stylesheet" href="css/build/styles.css">

	<!-- Vendor scripts -->
	<script src="js/build/required-scripts.js"></script>

	<!-- Natural learning scripts -->
	<script src="js/build/natural-learning.min.js"></script>

	<!-- select2 -->
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet"/>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

	<!-- scroll reveal -->
	<script src="vendor/scrollreveal.min.js"></script>
</head>
<body class="home">


<!-- first -->
<section class="hero-section bg-gradient color-white auto-height">
	<div style="background-image: url('/img/sample2.jpg');"
		class="hero-bg-image"></div>
	<div class="hero-content primary">
		<div class="container">
			<h1 class="hero-title"><span>Natural Learning</span></h1>
			<em class="font-serif">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, culpa laborelaborum
				magni.</em>

			<div>
				<br><br>
				<a href="#" class="btn btn-white btn-outline btn-radius text-light ">Get started</a>
			</div>
		</div>

	</div>
</section>

<!-- second -->
<section class="hero-section bg-content-secondary">
	<div class="hero-content">
		<div class="col-md-6">
			<div class="row">
				<img src="img/sample1.jpg" alt="" class="img-responsive center-block reveal-1">
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 text-left">
					<div class="heading">
						<h2 class="text-light">Consectetur adipisicing elit !</h2>
					</div>
					<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, culpa laborelaborum magni.</p>
					<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, eaque, excepturi? Aut maiores modi natus nihil, sed unde veniam.</p>
					<br>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="hero-section bg-primary">
	<div class="hero-content">
		<div class="heading">
			<h2 class="text-light">Consectetur adipisicing elit !</h2>
		</div>
		<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, culpa laborelaborum magni.</p>
		<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, eaque, excepturi? Aut maiores modi natus nihil, sed unde veniam.</p>
		<br>

		<div>
			<img src="img/mockup-1.png" alt="" class="img-responsive center-block">
		</div>
	</div>
</section>

<section class="hero-section bg-secondary color-white">
	<div class="hero-content">
		<div class="heading">
			<h2 class="text-light">Consectetur adipisicing elit !</h2>
		</div>
		<p class="color-white text-light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, culpa laborelaborum magni.</p>
		<p class="color-white text-light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, eaque, excepturi? Aut maiores modi natus nihil, sed unde veniam.</p>
		<br>

		<div>
			<img src="img/mockup-1.png" alt="" class="img-responsive center-block">
		</div>
	</div>
</section>


<script>

	window.sr = ScrollReveal();
	sr.reveal('.reveal-1', {
		origin: 'left',
		delay: 200,
		distance: '100px',
		easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
		scale: 1,
	});

	sr.reveal('.heading', {
		origin: 'bottom',
		delay: 200,
		distance: '10px',
		easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
		scale: 1,
	});

	sr.reveal('.hero-content p', {
		origin: 'top',
		delay: 600,
		distance: '10px',
		easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
		scale: 0.9,
		duration: 600,
	});



	sr.reveal('.hero-bg-image', {
		origin: 'top',
		delay:0,
		distance: '10px',
		easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
		scale: 1.5,
		duration: 400
	});

	sr.reveal('.hero-content.primary', {
		origin: 'top',
		delay: 500,
		distance: '10px',
		easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
		scale: 1.5,
		duration: 800
	});

</script>

</body>
</html>

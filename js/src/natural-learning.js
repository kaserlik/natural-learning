/*-------------------------------------
 | Scripts on document ready
 |------------------------------------
 |
 */
jQuery(document).ready(function () {

    // Responsive header menu button
    initHamburgerMenuButton();

    // init contextual buttons behavior
    initContextualButtons();

    // jPlayer sample video
    var $videoPlayer = jQuery("#jquery_jplayer_1");
    setVideoPlayer($videoPlayer, {
        ancestor: '#jp_container_1',
        title: 'Big Buck Bunny',
        m4v: "media/Big_Buck_Bunny_Trailer.m4v",
    });

    initTagFinder();

});


/**
 * Init jPlayer
 */
function setVideoPlayer($player, opts) {

    // Check if jPlayer is loaded in current page and $player element exists
    if ($player.length == 0 || typeof jQuery().jPlayer == 'undefined') {
        return false;
    }

    $player.jPlayer({
        ready: function () {
            jQuery(this).jPlayer("setMedia", opts);
            set_auto_width_jplayer();
        },
        size: {
            width: "100%",
            height: "auto"
        },
        cssSelectorAncestor: opts.ancestor,
        swfPath: "/vendor/jplayer",
        supplied: "m4v",
        useStateClassSkin: true,
        autoBlur: true,
        smoothPlayBar: true,
        keyEnabled: true,
        remainingDuration: true,
        toggleDuration: true
    });

    jQuery(window).on('resize', function () {
        set_auto_width_jplayer();
    });

    /**
     * Set jPlay auto width (16:9)
     */
    function set_auto_width_jplayer() {
        var $video_player = jQuery('.video-player');
        var vw = $video_player.width();
        $video_player.find('.jp-jplayer').css(
            {
                width: vw + 'px',
                height: (vw / (16 / 9)) + 'px',
            });
    }
}


/**
 * Contextual buttons
 */
function initContextualButtons() {
    var $contextual_buttons = jQuery('[data-role="contextual"]');
    var $contextual_buttons_close = jQuery('[data-role="contextual-close"]');
    $contextual_buttons.on('click', function (e) {
        e.preventDefault();
        var $target_selector = jQuery(this).data('target');
        var $target_element = jQuery($target_selector);

        // Show target element
        $target_element.show();
    });

    $contextual_buttons_close.on('click', function (e) {
        e.preventDefault();

        var $wrapper = jQuery(this).closest('.contextual-details');
        $wrapper.hide();
    });
}


/**
 * Header responsive menu behavior
 */
function initHamburgerMenuButton() {
    var $hamburgerButton = jQuery('.primary-header .hamburger');
    $hamburgerButton.on('click', function (e) {
        e.preventDefault();
        var $button = jQuery(this);
        $button.toggleClass('is-active');
        var $menu_primary = $button.closest('.primary-header ');
        $menu_primary.toggleClass('active');
    });
}


function initTagFinder() {

    jQuery("#main-tag-finder").select2({
        ajax: {
            url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                // !IMPORTANT! your every item in data.items has to have an .id property - this is the actual value that Select2 uses
                // Luckily the source data.items already have one
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup; // let our custom formatter work
        },
        minimumInputLength: 1,
        templateResult: function(repo) {
            if (repo.loading) return repo.text;
            return repo.full_name;
        },
        templateSelection: function(repo) {
            return repo.full_name || repo.text;
        }
    });


}
